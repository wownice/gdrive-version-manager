package Drive.Downloader;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Revision;

import Drive.Downloader.FileManager;
import Drive.Downloader.FileManager.FileProgress;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class UI 
{
	private Stage stage;
	private final Drive service;
	private FileManager manager;
	
	private String uploadDir;
	
	public UI(Stage st, Drive serv) throws IOException
	{
		stage = st;
		stage.setTitle("Google Drive Version Manager");
		
		service = serv;
		//uploadDir 
		
		Scene scene = new Scene(initializeUI());
		stage.setScene(scene);
		stage.setWidth(800);
		stage.setHeight(600);
		stage.show();	
	}
	
	public Pane initializeUI() throws IOException
	{
		Pane mainPane = new VBox();
		
		Menu settingsMenu = new Menu("Settings"); 
        MenuItem dlDirItem = new MenuItem("Set download directory");  
        settingsMenu.getItems().add(dlDirItem); 
        
        Menu helpMenu = new Menu("Help"); 
        MenuItem searchGuideItem = new MenuItem("Searching Files Guide");
        helpMenu.getItems().add(searchGuideItem); 

        MenuBar menuBar = new MenuBar(); 
        menuBar.getMenus().addAll(settingsMenu, helpMenu); 
		
		/* Contains search bar and list of files */
		VBox finderVBox = new VBox();
		
		/* Search bar */
		HBox searchHBox = new HBox();
		String[] searchOptions = {"name contains", "name = ", "Custom Condition"}; //conditions to use for searching
		ComboBox<String> searchOptionsDropDown = new ComboBox<String>(FXCollections.observableArrayList(searchOptions)); 
		searchOptionsDropDown.getSelectionModel().selectFirst();
		TextField searchField = new TextField();
		Button searchButton = new Button("Search");
		searchHBox.getChildren().addAll(searchOptionsDropDown, searchField, searchButton);
		HBox.setHgrow(searchField, Priority.ALWAYS);

		/* Tree Columns */
		TreeTableView<Object> treeTable = new TreeTableView<>(); //Use Object to be able to get data from both File and Revision
		treeTable.setShowRoot(false);
		treeTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		
		TreeTableColumn<Object, String> treeTableColumn1 = new TreeTableColumn<>("File Name");
		TreeTableColumn<Object, String> treeTableColumn2 = new TreeTableColumn<>("Revision");
		TreeTableColumn<Object, String> treeTableColumn3 = new TreeTableColumn<>("Last Modified");
		TreeTableColumn<Object, String> treeTableColumn4 = new TreeTableColumn<>("Size");
		//TreeTableColumn<Object, String> treeTableColumn5 = new TreeTableColumn<>("ID");
		
		/* File data used in tree rows */
		treeTableColumn1.setCellValueFactory(new TreeItemPropertyValueFactory<>("name"));
		treeTableColumn2.setCellValueFactory(new TreeItemPropertyValueFactory<>("originalFilename"));
		treeTableColumn3.setCellValueFactory(new TreeItemPropertyValueFactory<>("modifiedTime"));
		treeTableColumn4.setCellValueFactory(new TreeItemPropertyValueFactory<>("size"));
		//treeTableColumn4.setCellValueFactory(new TreeItemPropertyValueFactory<>("id"));

		//treeTable.getColumns().addAll(treeTableColumn1, treeTableColumn2, treeTableColumn3, treeTableColumn4, treeTableColumn5);
		treeTable.getColumns().addAll(treeTableColumn1, treeTableColumn2, treeTableColumn3, treeTableColumn4);
		treeTable.setColumnResizePolicy(TreeTableView.CONSTRAINED_RESIZE_POLICY); //Display only the columns specified (prevents empty extra column)

		finderVBox.getChildren().addAll(searchHBox, treeTable);

		HBox controlBox = new HBox(20);
		controlBox.setPadding(new Insets(5, 0, 5, 0));
		controlBox.setAlignment(Pos.CENTER);
		Button uploadRev = new Button("Upload");
		uploadRev.setTooltip(new Tooltip("Upload a new revision to the base file. Files will be uploaded in alphabetical order, so the last file will become the final revision. After 200 revisions, 'Keep Forever' option will no longer work."));
		Button downloadRev = new Button("Download");
		downloadRev.setTooltip(new Tooltip("Downloads all selected files. Selecting a base file will only download the latest revision."));
		Button deleteRev = new Button("Delete");
		deleteRev.setTooltip(new Tooltip("Deletes the selected files/revisions. When deleting revisions, one revision on the base file must remain. Files will be deleted by the order they are selected. Deleting a base file will also delete all revisions."));
		controlBox.getChildren().addAll(uploadRev, downloadRev, deleteRev);	

		//Put buttons into a list so that they can be enabled/disabled
		ArrayList<Button> buttons = new ArrayList<Button>();
		buttons.add(uploadRev);
		buttons.add(downloadRev);
		buttons.add(deleteRev);
		
		TableView<FileProgress> progressTable = new TableView<FileProgress>();
		TableColumn<FileProgress, String> progressColumn1 = new TableColumn<FileProgress, String>("File Name");
		TableColumn<FileProgress, String> progressColumn2 = new TableColumn<FileProgress, String>("Type");
		TableColumn<FileProgress, Double> progressColumn3 = new TableColumn<FileProgress, Double>("Progress");
		TableColumn<FileProgress, Double> progressColumn4 = new TableColumn<FileProgress, Double>("Status");
		
		progressTable.getColumns().addAll(progressColumn1, progressColumn2, progressColumn3, progressColumn4);
		progressTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		
		progressColumn1.setCellValueFactory(new PropertyValueFactory<>("name"));
		progressColumn2.setCellValueFactory(new PropertyValueFactory<>("type"));
		progressColumn3.setCellValueFactory(new PropertyValueFactory<>("progress"));
		progressColumn3.setCellFactory(ProgressBarTableCell.forTableColumn());
		progressColumn4.setCellValueFactory(new PropertyValueFactory<>("status"));
		
		mainPane.getChildren().addAll(menuBar, finderVBox, controlBox, progressTable);
		
		//Create filemanager to manage file processes. Takes progressTable to update any changes
		manager = new FileManager(service, progressTable);
		
		//If download directory setting text file exists, then set it in manager
		java.io.File directoryCheck = new java.io.File("./download_directory.txt");
		if(directoryCheck.exists())
		{
			FileReader fr = new FileReader("./download_directory.txt");
			BufferedReader reader = new BufferedReader(fr);
			String directory = reader.readLine();
			manager.setDownloadDirectory(directory);
			System.out.println("Setting download directory to " + directory);
			reader.close();
		}
		
		/* Attach events */
		
		searchButton.setOnAction(e ->
		{
			itemSearch(treeTable, searchOptionsDropDown.getSelectionModel().getSelectedItem(), searchField.getText(), searchButton);
		});
		
		searchField.setOnAction(e ->
		{
			if(!searchButton.isDisabled()) //Only search if button can be pressed
				itemSearch(treeTable, searchOptionsDropDown.getSelectionModel().getSelectedItem(), searchField.getText(), searchButton);
	    });
		
		//Set download directory by updating manager, then try to create a setting file that can be used when starting up
		dlDirItem.setOnAction(e ->
		{
			DirectoryChooser directoryChooser = new DirectoryChooser();
			directoryChooser.setInitialDirectory(new java.io.File(FileManager.DOWNLOAD_DIRECTORY));
			directoryChooser.setTitle("Set download directory");
			java.io.File directory = directoryChooser.showDialog(stage);
			
			if(directory != null)
			{
				String path = directory.getPath(); 
				System.out.println("Setting download directory to " + path);
				manager.setDownloadDirectory(path);
				
				try
				{
					java.io.File directoryFile = new java.io.File("./download_directory.txt");
					BufferedWriter writer = new BufferedWriter(new FileWriter(directoryFile.getAbsolutePath()));
					writer.write(path);
					writer.close();
				}
				catch(IOException e1)
				{
					System.out.println("Error with setting download directory " + path);
					e1.printStackTrace();
				}
			}
		});
		
		searchGuideItem.setOnAction(e ->
		{
			//Opens the searching file guide into a new browser window
			Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
		    {
		        try 
		        {
		            desktop.browse(new URI("https://developers.google.com/drive/api/v3/search-files"));
		        } 
		        catch (Exception e1)
		        {
		        	System.out.println("Error with opening file searching guide");
		            e1.printStackTrace();
		        }
		    }
		});
			
		/* Right click to download or delete */
		ContextMenu fileMenu = new ContextMenu();
		MenuItem downloadItem = new MenuItem("Download");
		MenuItem deleteItem = new MenuItem("Delete");
		fileMenu.getItems().addAll(downloadItem, deleteItem);
		
		//Put menu items into a list so that they can be enabled/disabled
		ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
		menuItems.add(downloadItem);
		menuItems.add(deleteItem);
		
		treeTable.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() 
		{
		    @Override
		    public void handle(MouseEvent t)
		    {
		    	ObservableList<TreeItem<Object>> selection = treeTable.getSelectionModel().getSelectedItems();
		    	
		    	updateButtonDisplay(selection.size(), buttons, menuItems);	    	
		    	
		        if(t.getButton() == MouseButton.SECONDARY)
		        {
		            fileMenu.show(treeTable, t.getScreenX(), t.getScreenY());
		        }
		        else
		        {
		        	fileMenu.hide();
		        }
		    }
		});
		
		treeTable.setOnSort(e -> 
		{ 	//Clear selection when sorting to prevent errors
		     if(treeTable.getSelectionModel().getSelectedIndices().size() > 1) 
		    	 treeTable.getSelectionModel().clearSelection(); 
		 });
			
		downloadItem.setOnAction(e ->
		{
			downloadSelection(treeTable.getSelectionModel().getSelectedItems(), progressTable);
		});
		
		deleteItem.setOnAction(e ->
		{	//Delete selected items then remove all selections
			deleteSelection(treeTable.getSelectionModel().getSelectedItems());
			treeTable.getSelectionModel().clearSelection();
			updateButtonDisplay(treeTable.getSelectionModel().getSelectedItems().size(), buttons, menuItems);
		});
		
		downloadRev.setOnAction(e ->
		{
			downloadSelection(treeTable.getSelectionModel().getSelectedItems(), progressTable);
		});
		
		deleteRev.setOnAction(e ->
		{	//Delete selected items then remove all selections
			deleteSelection(treeTable.getSelectionModel().getSelectedItems());
			treeTable.getSelectionModel().clearSelection();
			updateButtonDisplay(treeTable.getSelectionModel().getSelectedItems().size(), buttons, menuItems);
		});

		uploadRev.setOnAction(e ->
		{
			//Get the base file to know where to upload to
			
			TreeItem<Object> item = treeTable.getSelectionModel().getSelectedItem();
			
			//Give the item if current selection is just a base file.
			File f;
			if(item.getValue().getClass().getName().equals("com.google.api.services.drive.model.File"))
				f = (File) item.getValue();
			else //Otherwise get the parent of the revision, which should be a base file
				f = (File) item.getParent().getValue();
			
			uploadNewRevision(f, progressTable);
		});

		//Disable buttons on start
		updateButtonDisplay(treeTable.getSelectionModel().getSelectedItems().size(), buttons, menuItems);
		
		return mainPane;
	}
	
	//Downloads selected items, regardless of base file.
	public void downloadSelection(ObservableList<TreeItem<Object>> selection, TableView<FileProgress> progressTable)
	{
		if(createPrompt("Download", selection.size()))
		{
			String name = "";
			File baseFile = null;
			FileProgress fp = null;
			
			for(TreeItem<Object> s : selection)
			{
				//Get file names
				if(s.getValue().getClass().getName().equals("com.google.api.services.drive.model.File"))
				{
					baseFile = (File) s.getValue();
					name = baseFile.getName();
					fp = new FileProgress(name, "download", baseFile);
				}
				else
				{
					Revision rev = (Revision) s.getValue();
					baseFile = (File) s.getParent().getValue();
					name = rev.getOriginalFilename();
					fp = new FileProgress(name, "download", baseFile, rev);
				}
				progressTable.getItems().add(fp);
				manager.getQueue().add(fp);
			}	
			manager.start();
		}
	}
	
	//Takes a baseFile so that any selected files for upload will become its revisions.
	public void uploadNewRevision(File baseFile, TableView<FileProgress> progressTable)
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Select files for upload");
		
		//Set initial folder directory if upload was previously made
		if(uploadDir != null)
		{
			java.io.File path = new java.io.File(uploadDir);
			if(path.exists())
				fileChooser.setInitialDirectory(path);
		}
		
		java.util.List<java.io.File> selectedFiles = fileChooser.showOpenMultipleDialog(stage);
		if(selectedFiles != null) 
		{
			for(java.io.File f : selectedFiles)
			{
				FileProgress fp = new FileProgress(f.getName(), "upload", baseFile);
				fp.setUploadFile(f);
				progressTable.getItems().add(fp);
				manager.getQueue().add(fp);		
			}
			
			//Get directory of first file to use as default for next upload
			java.io.File firstFile = selectedFiles.get(0);
			uploadDir = firstFile.getAbsolutePath().substring(0, firstFile.getAbsolutePath().lastIndexOf(firstFile.getName()));
			
			manager.start();
		}
		else
			System.out.println("No valid files selected for upload");
	}
	
	//Deletes selected files/revisions.
	public void deleteSelection(ObservableList<TreeItem<Object>> sel)
	{	
		if(createPrompt("Delete", sel.size()))
		{
			System.out.println("Deleting " + sel.size() + " items.");
			
			//Make deep copy so that selected table item list won't be cleared out
			ObservableList<TreeItem<Object>> selection = FXCollections.observableArrayList();
			for(TreeItem<Object> s : sel)
			{
				selection.add(s);
			}
			
			FileManager.deleteFiles(service, selection);			
		}
	}
	
	//Search for files on a new thread to prevent gui freezing
	public void itemSearch(TreeTableView<Object> treeTable, String option, String s, Button searchButton)
	{
		//Create a root to contain search results
		TreeItem<Object> root = new TreeItem<>(null);	
		treeTable.setRoot(root);
					
		new FileManager.FileSearch(service, root, option, s, searchButton).start();
	}
	
	//Confirm whether or not to do something to files
	public boolean createPrompt(String title, int count)
	{
		Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	    alert.setTitle(title + " Selection");
	    alert.setHeaderText("Confirm selected items to " + title);
	    
	    String f = "files";
	    if(count == 1)
	    	f = "file";
	    alert.setContentText(title + " " + count + " " + f + "?");

	    Optional<ButtonType> result = alert.showAndWait();
	    return (result.get() == ButtonType.OK);
	}
	
	//Enables/disables buttons based on whether or not something is selected
	public void updateButtonDisplay(int selSize, ArrayList<Button> buttons, ArrayList<MenuItem> items)
	{
		//Disable all if nothing selected
		if(selSize < 1)
    	{
    		for(Button b : buttons)
    			b.setDisable(true);
    		
    		for(MenuItem m : items)
    			m.setDisable(true);
    	}
    	else
    	{
    		//Disable uploading if more than or equal to 200 revisions exist for a file
    		for(Button b : buttons)
    		{
    			if(selSize >= 200 && b.getText().equals("Upload"))
    				b.setDisable(true);
    			else
    				b.setDisable(false);
    		}
    		
    		for(MenuItem m : items)
    			m.setDisable(false);
    	}
	}
}
