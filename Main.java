package Drive.Downloader;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application
{

    public static void main(String[] args) 
    {
    	Application.launch(args);
    	
    }
    
    public void start(Stage stage) throws Exception
	{
		new UI(stage, AccountManager.getDriveService());
	}   
}
