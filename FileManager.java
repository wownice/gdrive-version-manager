package Drive.Downloader;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Revision;
import com.google.api.services.drive.model.RevisionList;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;

//Manages file upload, download
public class FileManager implements Runnable
{
	public static String DOWNLOAD_DIRECTORY = System.getProperty("user.home");
	
	//Stores currently running/waiting processes.
	private ArrayList<FileProgress> queue; //Arraylist for random access cancels
	private Thread thread;
	private TableView<FileProgress> progressTable; //Table to refresh updated data
	private boolean running; //Used to start/stop thread
	private Drive service;
	private Semaphore mutex;
	
	public FileManager(Drive d, TableView<FileProgress> tv) 
	{
		queue = new ArrayList<FileProgress>();
		progressTable = tv;
		service = d;
		running = false;
		mutex = new Semaphore(1);
	}
	
	public ArrayList<FileProgress> getQueue()
	{
		return queue;
	}
	
	public void start()
	{
		if(!running)
		{
			thread = new Thread(this);
			thread.setDaemon(true);
	        thread.start();
		}
	}
	
	@Override
	public void run() 
	{
		running = true;
		System.out.println("Starting processes.");
		
		while(!queue.isEmpty())
		{
			FileProgress fp = queue.get(0);
			fp.setStatus("In progress");
			if(fp.getType().equals("download"))
				downloadFile(service, fp, progressTable);
			else
				uploadFile(service, fp, progressTable);
			
			fp.setStatus("Complete");
			
			try
			{
				//Lock the queue in case files are added the moment one is finished processing
				mutex.acquire();
				queue.remove(0);
				mutex.release();
			}
			catch(InterruptedException e1)
			{		
				System.out.println("Mutex interrupted");
				e1.printStackTrace();
			}
			progressTable.refresh();
		}
		
		System.out.println("All processes complete.");
		running = false;
	}
		
	public void setDownloadDirectory(String s)
	{
		DOWNLOAD_DIRECTORY = s;
	}
	
	//Stores progress of upload/download for a file as well as necessary data to start the process
	public static class FileProgress
	{
		private SimpleStringProperty name; //File name
		private SimpleStringProperty type; //Upload or download
		private SimpleDoubleProperty progress; //Percentage uploaded/downloaded
		private SimpleStringProperty status; //Displays if file process is In progress, Complete, or In queue
		private java.io.File uploadFile; //File to upload, if type is Upload
		private File baseFile; //Base file to upload to, or to get revision download from
		private Revision rev; //Revision for if download is a revision. Works with baseFile to download Revision
			
		public FileProgress(String n, String t, File base, Revision r)
		{
			this(n, t, base);
			rev = r;
		}
		
		public FileProgress(String n, String t, File base)
		{
			name = new SimpleStringProperty(n);
			type = new SimpleStringProperty(t);
			progress = new SimpleDoubleProperty(0);
			status = new SimpleStringProperty("In queue"); 
			baseFile = base;
		}
		
		public Revision getRevision()
		{
			return rev;
		}
		
		public boolean isRevision() //Returns true if a revision id exists. Used to differentiate between file and revision
		{
			return rev != null;
		}
		
		public void setUploadFile(java.io.File f)
		{
			uploadFile = f;
		}
		
		public java.io.File getUploadFile()
		{
			return uploadFile;
		}
		
		public File getBaseFile()
		{
			return baseFile;
		}
		
		//Getter setters for tableview display
		
		public String getName()
		{
			return name.get();
		}
		
		public String getType()
		{
			return type.get();
		}
		
		public double getProgress()
		{
			return progress.get();
		}
		
		public void setProgress(double s)
		{
			progress.set(s);
		}
		
		public String getStatus()
		{
			return status.get();
		}
		
		public void setStatus(String s)
		{
			status.set(s);
		}
	}

	//Searches for files on a new thread. Can take a button so that the display can be updated.
	public static class FileSearch implements Runnable
	{
		private Thread thread;
		private TreeItem<Object> root;
		private String option;
		private String query;
		private Button button;
		private Drive service;
		
		public FileSearch(Drive s, TreeItem<Object> item, String opt, String q)
		{
			this(s, item, opt, q, null);
		}
		
		public FileSearch(Drive s, TreeItem<Object> item, String opt, String q, Button b)
		{
			root = item;
			option = opt;
			query = q;
			service = s;
			button = b;
			
			//If not custom condition, add the option before query. Has issues with any ' and " in option
			if(!option.equals("Custom Condition"))
			{
				query = option + " \'" + query + "\'";
			}
			
			//Add 'me' in owners to get only files that the account can manage
			query += " and 'me' in owners";
			System.out.println(query);
		}
		
		public void start()
		{
	        thread = new Thread(this);
	        thread.setDaemon(true);
	        thread.start();
	    }
		
		@Override
		public void run() 
		{
			if(button != null)
				button.setDisable(true);
			
			try 
			{
				TreeItem<Object> baseFile;
				FileList result;
	
				//Empty queries get last modified files		
				result = service.files().list().setQ(query).setPageSize(1000).setFields("nextPageToken, files(id, name, modifiedTime, mimeType, size)").execute();
				System.out.println("Searching for " + query);
				
				List<File> files = result.getFiles();
				
				for(File f : files)
				{
					//Get all files except google app type files (folders, docs, powerpoint, sheets, etc)
					if(!f.getMimeType().contains("application/vnd.google-apps"))
					{
						baseFile = new TreeItem<>(f);
						
						/*Get revisions for all files except: 
						 * google app (docs, slides, etc) files
						 */
						if(!f.getMimeType().contains("vnd.google-apps"))
						{
							RevisionList revisions = service.revisions().list(f.getId()).setFields("nextPageToken, revisions(id, originalFilename, modifiedTime, size)").execute();
							List<Revision> revs = revisions.getRevisions();
	
							//Only display revisions if there is more than 1
							if(revs.size() > 1)
							{
								TreeItem<Object> revItem;
								for(Revision r : revs)
								{
									revItem = new TreeItem<>(r);
									baseFile.getChildren().add(revItem);
								}
								
								f.setOriginalFilename(revs.size() + " revisions");
								//f.setName(f.getName() + " (" + revs.size() + " revisions)");
							}
						}
						
						root.getChildren().add(baseFile);
					}
				}
			} 
			catch (Exception e)
			{
				System.out.println("Error with file search");
				e.printStackTrace();
			}
			
			if(button != null)
				button.setDisable(false);
		}
	}
	
	//Listener to display upload progress. Takes fileprogress to update the progress
	public static class UploadProgressListener implements MediaHttpUploaderProgressListener
	{
		private FileProgress progress;
		private TableView<FileProgress> progressTable;
		
		public UploadProgressListener(FileProgress p, TableView<FileProgress> pt)
		{
			progress = p;
			progressTable = pt;
		}
		 @Override
	    public void progressChanged(MediaHttpUploader uploader) throws IOException
	    {		 
			  switch (uploader.getUploadState()) 
			  { 
			    case INITIATION_STARTED:
			      System.out.println("Initiation Started");
			      break;
			    case INITIATION_COMPLETE:
				  System.out.println("Initiation Completed");
				  break;
				case MEDIA_IN_PROGRESS:
				  System.out.println("Upload in progress. Percentage: " + uploader.getProgress());
				  progress.setProgress(uploader.getProgress());
				  progressTable.refresh();
				  break;
				case MEDIA_COMPLETE:
				  System.out.println("Upload Completed! Percentage: " + uploader.getProgress());
				  progress.setProgress(uploader.getProgress());
				  progressTable.refresh();
			      break;
				default:
					break;
			  }
	    }
	}
	
	//Listener to display download progress. Takes fileprogress to update the progress
	public static class DownloadProgressListener implements MediaHttpDownloaderProgressListener
	{
		private FileProgress progress;
		private TableView<FileProgress> progressTable;
		
		public DownloadProgressListener(FileProgress p, TableView<FileProgress> pt)
		{
			progress = p;
			progressTable = pt;
		}
		
	    @Override
	    public void progressChanged(MediaHttpDownloader downloader) throws IOException 
	    {
	        switch (downloader.getDownloadState())
	        {
	            case MEDIA_IN_PROGRESS:
	            	System.out.println("Download in progress: " + downloader.getProgress());
	            	progress.setProgress(downloader.getProgress());
	            	progressTable.refresh();
	                break;
	            case MEDIA_COMPLETE:
	            	System.out.println("Download complete: " + downloader.getProgress());
	            	progress.setProgress(downloader.getProgress());
	            	progressTable.refresh();
	                break;
	            case NOT_STARTED:
					System.out.println("Download not started");
					break;
	            default:
	            	System.out.println("nn");
	            	break;
	         }
	    }
	}
	
	//Revisions do not update their current progress, so progress has to be made off of the output stream
	//Two ways to set progress: MediaHttpDownloaderProgressListener's will show progress once completed, while the thread setter will show while downloading. The end result should be the same.
	public static class RevisionDownloadProgressListener implements MediaHttpDownloaderProgressListener, Runnable
	{
		private FileProgress progress;
		private TableView<FileProgress> progressTable;
		private FileOutputStream fileOutputStream;
		private Long size;
		private boolean running;
		private Thread thread;
		
		public RevisionDownloadProgressListener(FileProgress p, TableView<FileProgress> pt, FileOutputStream f, Long s)
		{
			progress = p;
			progressTable = pt;
			fileOutputStream = f;
			size = s;
		}
		
		public void start()
		{
			running = true;
			thread = new Thread(this);
			thread.setDaemon(true);
			thread.start();
		}
		
		public void printProgress()
		{
			if(running)
			{
				try
				{
					double total = ((double) (fileOutputStream.getChannel().size()))/size;
					System.out.println("Rev downloading, byte stream size: " + fileOutputStream.getChannel().size() + ". Overall:" + total);
					progress.setProgress(total);
					progressTable.refresh();
					
					TimeUnit.SECONDS.sleep(5);
					
					if(total < 1)
						printProgress();
					else
					{
						running = false;
						System.out.println("Rev download complete in printProgress");
					}
				}
				catch (InterruptedException | IOException e)
				{
					System.out.println("Revision time sleep interrupted or error with file");
					e.printStackTrace();
				}
			}
		}
		
	    @Override
	    public void progressChanged(MediaHttpDownloader downloader) throws IOException 
	    {
	        switch (downloader.getDownloadState())
	        {
	            case MEDIA_COMPLETE:
	            	System.out.println("Download complete: " + fileOutputStream.getChannel().size());
	            	progress.setProgress(1);
					progressTable.refresh();
	                break;
	            default:
	            	break;
	         }
	    }

		@Override
		public void run() 
		{
			printProgress();
		}
	}

	//Downloads a File or Revision
	public static void downloadFile(Drive service, FileProgress fp, TableView<FileProgress> progressTable)
	{
		File baseFile = fp.getBaseFile();
		
		String filename = "";
		FileOutputStream fileOutputStream;
		
		try 
		{
			if(fp.isRevision())
			{			
				Revision rev = fp.getRevision();
				filename = rev.getOriginalFilename();
				
				fileOutputStream = new FileOutputStream(new java.io.File(DOWNLOAD_DIRECTORY, filename));
				
				Drive.Revisions.Get revGet = service.revisions().get(baseFile.getId(), rev.getId());
				RevisionDownloadProgressListener revDownloadListener = new RevisionDownloadProgressListener(fp, progressTable, fileOutputStream, rev.getSize());
				revGet.getMediaHttpDownloader().setProgressListener(revDownloadListener).setChunkSize(3800000); //make chunk size customizable
				revDownloadListener.start();
				
				
				revGet.executeMediaAndDownloadTo(fileOutputStream);
		        fileOutputStream.close();
			}
			else
			{
				Drive.Files.Get fileGet = service.files().get(baseFile.getId());
				filename = baseFile.getName();
				
				fileOutputStream = new FileOutputStream(new java.io.File(DOWNLOAD_DIRECTORY, filename));
				
				MediaHttpDownloader fileDownloader = fileGet.getMediaHttpDownloader();
				fileDownloader.setProgressListener(new DownloadProgressListener(fp, progressTable)).setChunkSize(3800000);
				fileGet.executeMediaAndDownloadTo(fileOutputStream);
			}

	        fileOutputStream.close();
		} 
		catch(IOException e)
		{
			System.out.println("Error with downloading file " + filename);
			e.printStackTrace();
		}
	}
	
	//Takes a file to upload as a revision on the base file
	public static void uploadFile(Drive service, FileProgress fp, TableView<FileProgress> progressTable)
	{
		java.io.File file = fp.getUploadFile();
		File baseFile = fp.getBaseFile();
		
		System.out.println("Starting upload for " + file.getName());
		try 
		{					
			String headId = service.files().get(baseFile.getId()).setFields("headRevisionId").execute().getHeadRevisionId();
			
			//Create new file to store uploaded file in
			File fileMetadata = new File();
			fileMetadata.setOriginalFilename(file.getName());
			FileContent mediaContent = new FileContent("", file);
			
			//Upload revision, get result file
			Drive.Files.Update update = service.files().update(baseFile.getId(), fileMetadata, mediaContent);
			
			//Listener for upload progress
			MediaHttpUploader uploader = update.getMediaHttpUploader();
			uploader.setProgressListener(new UploadProgressListener(fp, progressTable));
			update.execute();

			//Get previous head revision and update it for Keep Forever
			Revision upRev = service.revisions().get(baseFile.getId(), headId).setFields("id").execute();//Only get id as other parts are not modifiable
			upRev.setKeepForever(true);
			upRev = service.revisions().update(baseFile.getId(), upRev.getId(), upRev).setFields("originalFilename, id, modifiedTime, keepForever").execute();

			System.out.println(file.getName() + " upload complete");
		}
		catch (IOException e)
		{
			System.out.println("Error with uploading " + file.getName());
			e.printStackTrace();
		}
	}

	//Deletes a selection of files. Takes an array of tree items so that either File or Revision can be differentiated. Does so on current thread rather than FileManager's
	public static void deleteFiles(Drive service, ObservableList<TreeItem<Object>> selection)
	{
		boolean deleted;
		
		for(TreeItem<Object> s : selection)
		{
			File baseFile = null;
			deleted = false;
			
			if(s.getValue().getClass().getName().equals("com.google.api.services.drive.model.File"))
			{
				baseFile = (File) s.getValue();
				
				try 
				{
					service.files().delete(baseFile.getId()).execute();
					deleted = true;
				} 
				catch (IOException e) 
				{
					System.out.println("Error with deleting file " + baseFile.getName());
					e.printStackTrace();
				}
			}
			else
			{
				baseFile = (File) s.getParent().getValue();
				Revision rev = (Revision) s.getValue();
				
				try 
				{
					service.revisions().delete(baseFile.getId(), rev.getId()).execute();
					deleted = true;
				} 
				catch (Exception e) 
				{
					System.out.println("Error with deleting revision " + rev.getOriginalFilename());
					e.printStackTrace();
				}
			}
			
			if(deleted)
				s.getParent().getChildren().remove(s);
		}
	}
}
