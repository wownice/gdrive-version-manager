Application for management of Google Drive files and their revisions

Can upload/delete/download revisions for existing files. Cannot upload files normally.

Issues
-----------
Currently has issues when searching with queries containing ' or "

Files with many revisions may result in 500 internal errors. This may freeze the UI.


[Searching for files](https://developers.google.com/drive/api/v3/search-files)